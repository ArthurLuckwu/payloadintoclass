package com.artlck.payloadintoclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayloadintoclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayloadintoclassApplication.class, args);
	}

}
