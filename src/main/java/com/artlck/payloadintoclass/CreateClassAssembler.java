package com.artlck.payloadintoclass;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.capitalize;

public class CreateClassAssembler {

    public static void generateClasses(String className, Object content, String path, String packageName) {
        className = capitalize(className);
        Map<String, Object> data = (Map<String, Object>) content;

        Map<String, Object> classes = data.entrySet().stream()
                .filter(t -> t.getValue().getClass().getSimpleName().equals("LinkedHashMap")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));

        Map<String, Object> fields = data.entrySet().stream()
                .filter(t -> !t.getValue().getClass().getSimpleName().equals("LinkedHashMap")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));

        Map<String, Object> arrayLists = data.entrySet().stream()
                .filter(t -> t.getValue().getClass().getSimpleName().equals("ArrayList")).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));

        String classText = createClass(className, fields, classes, packageName);
        createClassFile(className, classText, path);

        classes.entrySet().stream().forEach(clazz -> {
            generateClasses(clazz.getKey(), clazz.getValue(), path, packageName);
        });

        arrayLists.entrySet().stream().forEach(clazz -> {
            if (((ArrayList) clazz.getValue()).get(0).getClass().getSimpleName().equals("LinkedHashMap")) {
                Map<String, Object> values = new HashMap<>();
                ((ArrayList) clazz.getValue()).forEach(v -> {
                    values.putAll((LinkedHashMap) v);
                });
                generateClasses(clazz.getKey(), values, path, packageName);
            }
        });
    }

    private static String createClass(String className, Map<String, Object> fields, Map<String, Object> classes, String packageName) {
        AtomicReference<String> clazz = new AtomicReference<>("");
        if (packageName != null) {
            clazz.set(clazz.get().concat("package ").concat(packageName).concat(";\n\n"));
        }
        clazz.set(clazz.get().concat("public class ").concat(className).concat(" { \n"));

        fields.forEach((key, value) -> {
            String fieldType = value.getClass().getSimpleName();
            if (fieldType.equals("ArrayList")) {
                String listType = key;
                if (!((ArrayList) value).get(0).getClass().getSimpleName().equals("LinkedHashMap")) {
                    listType = ((ArrayList) value).get(0).getClass().getSimpleName();
                }
                fieldType = "List".concat("<").concat(capitalize(listType)).concat(">");
            }
            clazz.set(clazz.get().concat(createField(key, fieldType)));
        });

        classes.forEach((key, value) -> {
            clazz.set(clazz.get().concat(createField(key, capitalize(key))));
        });

        fields.forEach((key, value) -> {
            String fieldType = value.getClass().getSimpleName();
            if (fieldType.equals("ArrayList")) {
                String listType = key;
                if (!((ArrayList) value).get(0).getClass().getSimpleName().equals("LinkedHashMap")) {
                    listType = ((ArrayList) value).get(0).getClass().getSimpleName();
                }
                fieldType = "List".concat("<").concat(capitalize(listType)).concat(">");
            }
            clazz.set(clazz.get().concat(createGetAndSetters(key, fieldType)));
        });

        classes.forEach((key, value) -> {
            clazz.set(clazz.get().concat(createGetAndSetters(key, capitalize(key))));
        });

        return clazz.get().concat(" \n}");
    }

    private static String createField(String fieldName, String fieldType) {
        if (fieldType.equals("ArrayList")) {
            fieldType = "List".concat("<").concat(capitalize(fieldName)).concat(">");
        }
        return "  private ".concat(fieldType).concat(" ").concat(fieldName).concat(";\n");
    }

    private static String createGetAndSetters(String fieldName, String fieldType) {
        if (fieldType.equals("ArrayList")) {
            fieldType = "List".concat("<").concat(capitalize(fieldName)).concat(">");
        }
        String get = "\n  public ".concat(fieldType);
        get = get.concat(" get").concat(capitalize(fieldName)).concat("() {\n");
        get = get.concat("    ");
        get = get.concat("return ").concat(fieldName).concat(";\n");
        get = get.concat("  }\n");

        String set = "\n  public void set";
        set = set.concat(capitalize(fieldName)).concat("(").concat(fieldType);
        set = set.concat(" ").concat(fieldName).concat(") {\n");
        set = set.concat("    ");
        set = set.concat("this.").concat(fieldName).concat(" = ");
        set = set.concat(fieldName).concat(";\n");
        set = set.concat("  }\n");

        return get.concat(set);
    }

    private static void createClassFile(String name, String content, String path) {
        try {
            if (!path.substring(path.length() - 1).equals("\\")) {
                path = path.concat("\\");
            }
            if (!path.substring(path.length() - 1).equals("/")) {
                path = path.concat("/");
            }
            FileWriter file = new FileWriter(path.concat(name).concat(".java"));
            PrintWriter recordFile = new PrintWriter(file);
            recordFile.print(content);
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
