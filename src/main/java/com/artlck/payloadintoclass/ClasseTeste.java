package com.artlck.payloadintoclass;

public class ClasseTeste {
  private String teste;
  private Integer numero;

  public String getTeste() {
    return teste;
  }

  public void setTeste(String teste) {
    this.teste = teste;
  }

  public Integer getNumero() {
    return numero;
  }

  public void setNumero(Integer numero) {
    this.numero = numero;
  }
 
}