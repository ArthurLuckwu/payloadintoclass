package com.artlck.payloadintoclass;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/conversor")
public class PayloadIntoClassController {

    @PostMapping
    public String convert(@RequestParam() String requestName,
                          @RequestParam String absolutePath,
                          @RequestParam(required = false) String packageName,
                          @RequestBody Object content) {
        CreateClassAssembler.generateClasses(requestName, content, absolutePath, packageName);
        return "Sucesso!";
    }

}
